# PiHole Maintenance Ansible Playbook

Playbook optimized to update both Raspian [Debian] OS and PiHole Gravity lists and PiHole core system. 


This playbook is intended to be run on the local environment (localhost) from the Raspberry Pi itself, however this can easily be changed to run remotely by simply changing out the respective hosts. 

Assuming the Raspberry Pi is already running PiHole over Raspian, we are first going to add the following line to /etc/apt/sources.list to add the PPA to our Debian update list:

`deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main`

Next, validate, update, and install by running:

```
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
$ sudo apt update
$ sudo apt install ansible
```

From here, you can simply validate the playbook with:

`$ ansible-playbook raspupdate.yml --check`

and to run manually from within the project directory:

`$ ansible-playbook raspupdate.yml`

*Edit crontab to schedule playbooks* (Optional)

Navigate to `etc/crontab`
Add a line in crontab (nano or vim), for example to run this playbook weekly every Sunday at midnight add the following line:

```
0 0     * * 0   root    if ! out=`ansible-playbook /home/pi/Ansible/raspupdate.yml`; then echo $out; fi
```
